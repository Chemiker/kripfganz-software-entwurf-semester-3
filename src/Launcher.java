import java.io.File;
import java.io.IOException;

import model.ComponentGraph;
import model.Graph;
import model.DfsGraph;

/**
 * @author Nicky Gruner
 */
public class Launcher {

    private static String file;
    private static Comparable root;
    private static boolean convert = false;

    public static void main (String[] args) {
        processArguments(args);
        try {
            Graph graph = Graph.loadFromFile(new File(file), convert);
            System.out.println("First DFS:");
            DfsGraph dfs = DfsGraph.search(graph, root);
            dfs.printRoots();
            System.out.println("\nSecond (transposed) DFS:");
            ComponentGraph componentGraph = ComponentGraph.generate(graph, dfs);
            componentGraph.printComponents();
            System.out.println("component graph:");
            componentGraph.printComponentGraph();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processArguments (String[] args) {
        switch (args.length) {
            case 3:
                if (!"-n".equals(args[0].toLowerCase())) {
                    showUsage(2);
                }
                convert = true;
                args[0] = args[1];
                args[1] = args[2];
            case 2:
                file = args[0];
                if (convert) {
                    root = new Integer(args[1]);
                } else {
                    root = args[1];
                }
                break;
            default:
                showUsage(1);
        }
    }

    private static void showUsage (int exitCode) {
        System.err.println("Usage: java -jar Kripfganz.jar [-n] filename root");
        System.exit(exitCode);
    }

}

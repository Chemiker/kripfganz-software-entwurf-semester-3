package model;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author Nicky Gruner
 */
public class ComponentGraph extends DfsGraph {

    private static final Comparator<Comparable> PRIORITY = (c1, c2) -> c2.compareTo(c1);

    public static ComponentGraph generate (Graph graph, DfsGraph dfsGraph) {
        Graph transposed = graph.transpose();
        SortedMap<Integer, Node> priorityMap = new TreeMap<>(PRIORITY);
        dfsGraph.getDfei().forEach((node, dfei) -> priorityMap.put(dfei, transposed.getNodes().get(node.getValue())));
        Collection<Node> priority = priorityMap.values();
        ComponentGraph result = new ComponentGraph();
        transposed.nodes.values().forEach(node -> {
            result.dfbi.put(node, Integer.MAX_VALUE);
            result.dfei.put(node, Integer.MAX_VALUE);
        });
        List<Node> stack = new LinkedList<>();
        int i = 0;
        int j = 0;
        Optional<Node> opt = result.getNextRoot(priority);
        while (opt.isPresent()) {
            Node root = opt.get();
            Component component = new Component(root);
            result.addNode(component);
            result.dfbi.put(root, ++i);
            stack.add(0, root);
            while (!stack.isEmpty()) {
                Node v = stack.get(0);
                Optional<Node> s = v.getSuccessors().stream().filter(successor -> result.dfbi.get(successor) == Integer.MAX_VALUE).findFirst();
                if (s.isPresent()) {
                    Node w = s.get();
                    result.dfbi.put(w, ++i);
                    component.addEdge(v.getValue(), w.getValue());
                    stack.add(0, w);
                } else {
                    stack.remove(0);
                    result.dfei.put(v, ++j);
                }
            }
            opt = result.getNextRoot(priority);
        }
        i = 0;
        for (Edge edge : graph.getEdges()) {
            if (i >= result.nodes.size()) {
                break;
            }
            Component original = (Component) result.nodes.values().stream().filter(node -> ((Component) node.getValue()).containsNode(edge.getOrigin())).findFirst().get().getValue();
            Component destination = (Component) result.nodes.values().stream().filter(node -> ((Component) node.getValue()).containsNode(edge.getDestination())).findFirst().get().getValue();
            if (!original.equals(destination)) {
                i++;
                result.addEdge(original, destination);
            }
        }
        return result;
    }

    private Optional<Node> getNextRoot (Collection<Node> priority) {
        return priority.stream().filter(node -> dfbi.get(node) == Integer.MAX_VALUE).findFirst();
    }

    public void printComponents () {
        nodes.values().forEach(c -> {
            Component component = (Component) c.getValue();
            System.out.printf("component %d:%nroot: %s%nnodes: %s%n%n", component.getId(), component.getRoot(), component);
        });
    }

    public void printComponentGraph () {
        getEdges().forEach(edge -> System.out.printf("%s\t->\t%s%n", ((Component) edge.getOrigin().getValue()).getId(), ((Component) edge.getDestination().getValue()).getId()));
    }

    private static class Component extends Graph implements Comparable<Component> {

        private static int nextId = 1;

        private final Node root;
        private final int id = nextId++;

        public Component (Node root) {
            this.root = root;
            addNode(root.getValue());
        }

        public int getId () {
            return id;
        }

        public Node getRoot () {
            return root;
        }

        public boolean containsNode (Node node) {
            return nodes.containsKey(node.getValue());
        }

        @Override
        public int compareTo (Component o) {
            return id - o.id;
        }
    }

}

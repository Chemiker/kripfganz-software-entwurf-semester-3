package model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Nicky Gruner
 */
public class DfsGraph extends Graph {

    protected final Map<Node, Integer> dfbi;
    protected final Map<Node, Integer> dfei;

    protected DfsGraph () {
        dfbi = new HashMap<>();
        dfei = new HashMap<>();
    }

    public Map<Node, Integer> getDfbi () {
        return dfbi;
    }

    public Map<Node, Integer> getDfei () {
        return dfei;
    }

    public static DfsGraph search (final Graph graph, final Comparable searchRoot) {
        DfsGraph result = new DfsGraph();
        graph.nodes.values().forEach(node -> {
            result.dfbi.put(node, Integer.MAX_VALUE);
            result.dfei.put(node, Integer.MAX_VALUE);
        });
        List<Node> stack = new LinkedList<>();
        int i = 0;
        int j = 0;
        Optional<Node> opt = Optional.of(graph.nodes.get(searchRoot));
        while (opt.isPresent()) {
            Node root = opt.get();
            result.addNode(root.getValue());
            result.dfbi.put(root, ++i);
            stack.add(0, root);
            while (!stack.isEmpty()) {
                Node v = stack.get(0);
                Optional<Node> s = v.getSuccessors().stream().filter(successor -> result.dfbi.get(successor) == Integer.MAX_VALUE).findFirst();
                if (s.isPresent()) {
                    Node w = s.get();
                    result.dfbi.put(w, ++i);
                    result.addEdge(v.getValue(), w.getValue());
                    stack.add(0, w);
                } else {
                    stack.remove(0);
                    result.dfei.put(v, ++j);
                }
            }
            opt = graph.nodes.values().stream().filter(node -> result.dfbi.get(node) == Integer.MAX_VALUE).findFirst();
        }
        return result;
    }

    public void printRoots () {
        System.out.print("roots:");
        getNodes().values().stream().filter(node -> node.getPredecessors().isEmpty()).forEach(node -> System.out.printf("\t%s", node.getValue()));
        System.out.println();
    }

    public void printDfbiAndDfei () {
        System.out.println("Node|DFBI|DFEI");
        System.out.println("----+----+----");
        dfbi.forEach((node, dfbi) -> System.out.printf("%4s|%4s|%4s%n", node, dfbi, dfei.get(node)));
    }

}

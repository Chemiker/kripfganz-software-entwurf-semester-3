package model;

/**
 * @author Nicky Gruner
 */
public class Edge {

    private final Node origin;
    private final Node destination;

    public Edge (Node origin, Node destination) {
        this.origin = origin;
        this.destination = destination;
        origin.addSuccessor(destination);
        destination.addPredecessor(origin);
    }

    public Node getOrigin () {
        return origin;
    }

    public Node getDestination () {
        return destination;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Edge)) {
            return false;
        }

        Edge edge = (Edge) o;

        if (!destination.equals(edge.destination)) {
            return false;
        }
        if (!origin.equals(edge.origin)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode () {
        int result = origin.hashCode();
        result = 31 * result + destination.hashCode();
        return result;
    }

    @Override
    public String toString () {
        return String.format("%2s -> %2s", origin, destination);
    }
}

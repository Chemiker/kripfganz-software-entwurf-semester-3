package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author Nicky Gruner
 */
public class Graph {

    protected final Map<Comparable, Node> nodes;
    private final Set<Edge> edges;

    protected Graph () {
        nodes = new TreeMap<>();
        edges = new LinkedHashSet<>();
    }

    public static Graph loadFromFile (File file, boolean convert) throws IOException {
        Graph graph = new Graph();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("\t");
                if (convert) {
                    graph.addEdge(new Integer(parts[0]), new Integer(parts[1]));
                } else {
                    graph.addEdge(parts[0], parts[1]);
                }
            }
        }
        return graph;
    }

    public Graph transpose () {
        Graph transposed = new Graph();
        edges.forEach(edge -> transposed.addEdge(edge.getDestination().getValue(), edge.getOrigin().getValue()));
        return transposed;
    }

    public Map<Comparable, Node> getNodes () {
        return Collections.unmodifiableMap(nodes);
    }

    public Set<Edge> getEdges () {
        return Collections.unmodifiableSet(edges);
    }

    protected Node addNode (Comparable value) {
        if (!nodes.containsKey(value)) {
            nodes.put(value, new Node(value));
        }
        return nodes.get(value);
    }

    protected boolean addEdge (Comparable origin, Comparable destination) {
        Node from = addNode(origin);
        Node to = addNode(destination);
        return edges.add(new Edge(from, to));
    }

    public void printEdges () {
        edges.forEach(System.out::println);
    }

    @Override
    public String toString () {
        StringBuilder builder = new StringBuilder("{");
        if (!nodes.isEmpty()) {
            nodes.keySet().forEach(node -> builder.append(node).append(','));
            builder.deleteCharAt(builder.length() - 1);
        }
        builder.append('}');
        return builder.toString();
    }
}

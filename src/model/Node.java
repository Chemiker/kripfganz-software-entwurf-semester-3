package model;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Nicky Gruner
 */
public class Node implements Comparable<Node>, Cloneable {

    private final Comparable value;

    private final Set<Node> successors;
    private final Set<Node> predecessors;

    public Node (Comparable value) {
        this.value = value;
        successors = new TreeSet<>();
        predecessors = new TreeSet<>();
    }

    public Comparable getValue () {
        return value;
    }

    boolean addSuccessor (Node node) {
        return successors.add(node);
    }

    boolean removeSuccessor (Node o) {
        return successors.remove(o);
    }

    boolean addPredecessor (Node node) {
        return predecessors.add(node);
    }

    boolean removePredecessor (Node o) {
        return predecessors.remove(o);
    }

    public Collection<Node> getSuccessors () {
        return Collections.unmodifiableCollection(successors);
    }

    public Collection<Node> getPredecessors () {
        return Collections.unmodifiableCollection(predecessors);
    }

    public void printSuccessorsAndPredecessors () {
        System.out.printf("Successors: %s%nPredecessors: %s%n", successors, predecessors);
    }

    @Override
    public Node clone () {
        return new Node(value);
    }

    @Override
    public int compareTo (Node o) {
        return value.compareTo(o.value);
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Node)) {
            return false;
        }

        Node node = (Node) o;

        if (!value.equals(node.value)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode () {
        return value.hashCode();
    }

    @Override
    public String toString () {
        return value.toString();
    }
}
